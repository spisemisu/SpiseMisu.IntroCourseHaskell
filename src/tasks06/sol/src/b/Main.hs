#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package aeson
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE Safe          #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Aeson
    ( ToJSON
    , encode
    )
import           GHC.Generics

--------------------------------------------------------------------------------

data Person = Person
  { name :: String
  , age  :: Int
  } deriving (Generic, Show)

--------------------------------------------------------------------------------

instance ToJSON Person

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  do
    putStrLn "Person \"John Doe\" 42 == { \"name\":\"John Doe\", \"age\":12 }"
    putStrLn $ show $ encode person
    where
      person = Person "John Doe" 42

{-

/introHaskell/SpiseMisu.IntroCourseHaskell/src/tasks06/sol/src/b/Main.hs:23:1:
error:
    Data.Aeson: Can't be safely imported! The module itself isn't safe.
   |
23 | import           Data.Aeson
   | ^^^^^^^^^^^^^^^^^^^^^^^^^^^...

-}
