#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --package time
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Time.Clock
    ( getCurrentTime
    )
import           Data.Time.Format
    ( defaultTimeLocale
    , formatTime
    )

--------------------------------------------------------------------------------

iso8601
  :: IO String

main
  :: IO ()

--------------------------------------------------------------------------------

iso8601 =
  getCurrentTime >>= pure . (formatTime defaultTimeLocale "%FT%T%0QZ")

main =
  do
    putStrLn "Current UTC DateTime (ISO-8601 formatted)"
    putStrLn =<< iso8601
