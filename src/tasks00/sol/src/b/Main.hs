module Main (main) where

--------------------------------------------------------------------------------

import           Prelude hiding
    ( fst
    , last
    )

--------------------------------------------------------------------------------

type First  = String
type Last   = String
data Person = Name First Last

data Person' = Name'
  { first :: String
  , last  :: String
  }

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  do
    putStrLn $  fst       ++ " " ++  lst
    putStrLn $ (first p2) ++ " " ++ (last p2)
  where
    (Name fst lst) = Name "John" "Doe"
    p2 = Name' "Juan" "De La Cierva"
