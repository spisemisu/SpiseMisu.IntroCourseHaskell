#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

class Monad m => ConsoleOutM m where
  consoleOut :: String -> m ()

class Monad m => ConsoleGetM m where
  consoleGet :: m String

--------------------------------------------------------------------------------

instance ConsoleOutM IO where
  consoleOut = putStrLn

instance ConsoleGetM IO where
  consoleGet = getLine

--------------------------------------------------------------------------------

granulated
  ::
    ( ConsoleGetM m
    , ConsoleOutM m
    )
  => m ()

main
  :: IO ()

--------------------------------------------------------------------------------

granulated =
  consoleGet >>= consoleOut . ("You typed: " ++) >> granulated

main =
  granulated
